extends Area

#===CONFIGURATIONS
const BLIP_SPEED = 0.15


#===SAVE DATA
#STATUS
var enemy_ally = "ally"#"ally" or "enemy"
var type

#INSIDE THE CASTLE
var captain
var army
var level
var kingdom
var effects
var prisioners
var soldiers

#extra stuff
var elapsed_seconds = 0

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	if Global.selected_castle == self:
		elapsed_seconds += delta
		if elapsed_seconds > BLIP_SPEED:
			elapsed_seconds = 0
			selected()

func selected():
		if self.get_node("View").opacity == 1:
			self.get_node("View").opacity = 0.5
		else:
			self.get_node("View").opacity = 1

func _input_event(camera, event, click_position, click_normal, shape_idx):
	#print("teste")
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			#var global = get_node("/root/Global")
			if Global.selected_army:
				Global.selected_army.destination = self
				Global.selected_army = null
				#print("army destination set")
				#get_node("/root/Army")
			else:
				Global.selected_castle = self
				print("castle selected")
				get_tree().paused = true
				#_my_level_was_completed()
				show_menu()
				
func show_menu():
	var menu = get_node("CastleMenu")
	menu.visible = true
	get_tree().paused = true

func _my_level_was_completed():
	get_tree().change_scene("res://scenes/Start.tscn")
			
			
	
func _mouse_enter():
	self.get_node("View").opacity = 0.5
	
func _mouse_exit():
	self.get_node("View").opacity = 1
	
func on_area_enter():
	print("entered!!!!!!!!!!!!!!!")


func _on_CastleList_item_selected(index):
	var menu_list = get_node("CastleMenu/CastleList")
	print(menu_list.get_item_text(index))
	if menu_list.get_item_text(index) == "Exit":
		get_tree().paused = false
		get_node("CastleMenu").visible = false
		self.get_node("View").opacity = 1
