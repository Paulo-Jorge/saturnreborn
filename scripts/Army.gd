extends Area

#===CONFIGURATIONS
const BLIP_SPEED = 0.2
#const VELOCITY = 0.005
const VELOCITY = 1
const CAM_FOCUS_VELOCITY = 35
const CAM_FOCUS_DISTANCE = 4

#===SAVE DATA
var destination
var captain
var members
#no need for captai: the 1st member will e the captain
#var captain

#extra stuff
var collision
#var battle_result# = won / lost
var move_value = 0
var elapsed_seconds = 0

func _ready():
	add_to_group("Ally_armies")

func _input(e):
	if e is InputEventMouseButton:
		if e.button_index == BUTTON_LEFT:
			pass


func _process(delta):
	if Global.selected_army == self:
		elapsed_seconds += delta
		if elapsed_seconds > BLIP_SPEED:
			elapsed_seconds = 0
			selected()
	if destination:
		move(delta)
	if collision:
		center_camera(delta, get_transform().origin)

func selected():
	if self.get_node("View").opacity == 1:
		self.get_node("View").opacity = 0.5
	else:
		self.get_node("View").opacity = 1

func move(delta):
	var start = get_transform().origin
	var end = destination.get_transform().origin
	#BECAUSE WE WANT Y ALWAYS CONSTANT, OTHERWISE IT WILL MOVE TO THE CENTER Y OF THE DESTIN OBJECT
	end.y = start.y
	
	linear_interpolation(delta, self, start, end, VELOCITY)
	animation(start,end)
	collision()
	#var collision = collision()
	#if collision:
	#	print("pumba coliso")
	#	center_camera(delta, start)
		
		
	
func animation(start,end):
	if(not self.get_node("View").is_playing()):
		self.get_node("View").opacity = 1
		#if true = is moving W
		if start.x < end.x:
			self.get_node("View").flip_h = false
			self.get_node("View").play("W")
		else:
			self.get_node("View").flip_h = true
			self.get_node("View").play("W")
			
func collision():
	#COLLISION
	if get_overlapping_areas():
	#if self.child("CollisionShape").is_colliding():
		self.get_node("View").stop()
		
		print("yeyyy chegou")
		print(get_overlapping_areas()[0].get_name())
		
		destination = null
		collision = get_overlapping_areas()[0]
		#get_tree().paused = true
		
		
	#	return get_overlapping_areas()
	#else:
	#	return false
		#center_camera(get_transform().origin)
		
		#DELETES THE OBJECT FROM THE NODES:
		#queue_free()
	
func center_camera(delta, position):
	var camera = get_node("/root/World/Camera")
	#var start = camera.get_transform().origin
	var start = camera.translation
	var end = position
	#TO MANTAIN CAMERA ZOOM LEVEL
	end.y = 3
	end.z = end.z + CAM_FOCUS_DISTANCE
	
	if (start).distance_to(end) < 1:
		#collision = null
		battle_screen()
	else:
		linear_interpolation(delta, camera, start, end, CAM_FOCUS_VELOCITY)
	
	
func linear_interpolation(delta, obj, start, end, velocity, y=null):
	#var start = get_transform().origin
	#var end = destination.get_transform().origin
	#end.z = end.z + 0.4
	#end.y = start.y
	var dir = (end - start).normalized()
	#OPTIMIZE this line. Maybe not necessary since we check collisions?
	if (start).distance_to(end) > 1:
		var motion = dir * (velocity * delta)
		obj.translate(motion)

func battle_screen():
	var battle_screen = get_node("Battle_Screen")
	battle_screen.visible = true
	get_tree().paused = true

#battle_result = "won" or "lost"
func after_battle(battle_result):
	
	var enemy_type = collision.get_parent().get_name()
	
	if battle_result == "lost":
		print("after battle lost")
		
		if enemy_type == "Castles":
			pass
		
	if battle_result == "won":
		#self.call_deferred('free')
		#print(get_name())
		self.queue_free()
		get_tree().paused = false
		print("after battle won")
	collision = null
		
	

"""func battle_screen():
	print("BATTLE!")
	#get_tree().paused = true
	get_tree().change_scene("res://scenes/Battle_Screen.tscn")"""
	
	
"""func go_to(delta):
	var t = get_global_transform()
	var start = t.origin
	var end = destination.get_global_transform().origin
	#self.move(end * VELOCITY * delta)
	var dir = (end - start.normalized())
	#dir.z = end.z + 2.6
	print(dir)
	#if dir < end:
	global_translate(dir*(delta*VELOCITY))
	
func linear_interpolation2(delta):
	var t = get_global_transform()
	#var start = Vector3(0,0,0)
	var start = t.origin
	#var end = Vector3(10,0.5,0)
	#var end = get_tree().get_root().get_node("/Castle/CollisionShape/Castle_Sprite/").get_transform().origin
	#var end = get_node("/root/World/Castle").get_global_transform().origin
	var end = destination.get_global_transform().origin
	var pos = start.linear_interpolate(end,move_value)
	move_value += delta * VELOCITY
	t.origin = pos
	set_transform(t)
	#print(delta)
	#if start >= end:
	#	print("yeyyy arrived")
	#	destination = null
	if (t.origin).distance_to(end) < 1.2:
		#lerp = false
		print("yeyyy arrived")
		destination = null"""

func _input_event(camera, event, click_position, click_normal, shape_idx):
	#print("teste")
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			#var global = get_node("/root/Global")
			#global.selected_army = self.get_path()
			Global.selected_army = self
			#global.selected_army.opacity = 0.3
			#self.get_node("View").modulate = Color(0,0,1)
			#print(get_node(global.selected_army).get_node("View").modulate)
			#print(global.selected_army.get_node("View").modulate)
			
func _mouse_enter():
	self.get_node("View").opacity = 0.5
	
func _mouse_exit():
	self.get_node("View").opacity = 1

"""func _on_Army_area_entered(area):
	print("area entered")"""




