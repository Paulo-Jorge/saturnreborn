extends Spatial

onready var army = preload("res://scenes/Army.tscn")
onready var armies = get_node("Armies")

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	#spawn_armies()
	pass

func spawn_armies():
	for i in range(4):
		var a = army.instance()
		armies.add_child(a)
		a.global_translate(Vector3(2*(i+1),0,2*(i+1)))

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

"""
func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed == true:
			if Global.selected_army:
				#global.selected_army.get_node("View").modulate = Color(1,1,1)
				Global.selected_army.get_node("View").opacity = 1
				Global.selected_army = null
				print("army deselected")
			if Global.selected_castle:
				Global.selected_castle = null
				print("castle deselected")"""

func _on_CastleList_item_selected(index):
	pass # replace with function body
