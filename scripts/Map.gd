extends Area

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			print("world map clicked")
			if Global.selected_army:
				#global.selected_army.get_node("View").modulate = Color(1,1,1)
				Global.selected_army.get_node("View").opacity = 1
				Global.selected_army = null
				print("army deselected")
			if Global.selected_castle:
				Global.selected_castle.get_node("View").opacity = 1
				Global.selected_castle = null
				print("castle deselected")
